package ${packageName};

import ${fullClassName};
import dev.tacon.beans.annotation.BeanProperty.RootBeanProperty;

/**
 * Class {@linkplain ${classPackageName}.${className}} metadata.
 * <p>
 * Generated with tacon.dev by Azserve
 */
@SuppressWarnings({"all"})
public final class ${className}${classSuffix} {

	<#list fields as field>
	/**
	 * Property {@code ${field.name}}.
	 */
	public static final RootBeanProperty<${className}, ${field.type}> ${field.name} = new RootBeanProperty<>(${className}.class, "${field.name}", ${field.classType}.class);

	</#list>

	private ${className}${classSuffix}() {}

	<#list innerClasses as innerClass>
${innerClass}
	</#list>
}