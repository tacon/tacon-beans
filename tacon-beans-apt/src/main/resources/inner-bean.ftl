	public static final class ${className}${classSuffix} {

		<#list fields as field>
		/**
		 * Property {@code ${field.name}}.
		 */
		public static final RootBeanProperty<${fullClassName}, ${field.type}> ${field.name} = new RootBeanProperty<>(${fullClassName}.class, "${field.name}", ${field.classType}.class);

		</#list>

		private ${className}${classSuffix}() {}

		<#list innerClasses as innerClass>
			${innerClass}
		</#list>
	}