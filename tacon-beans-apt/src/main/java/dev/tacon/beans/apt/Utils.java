package dev.tacon.beans.apt;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

public final class Utils {

	public static String getFieldName(final String methodName) {
		if (methodName.startsWith("get") || methodName.startsWith("set")) {
			return methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
		}
		if (methodName.startsWith("is")) {
			return methodName.substring(2, 3).toLowerCase() + methodName.substring(3);
		}
		return "";
	}

	public static String removeType(final String typeName) {
		final int indexOfLt = typeName.indexOf('<');
		if (indexOfLt > 0) {
			// XXX from "List<List<String>>[]" to "List[]"
			final int lastIndexOfGt = typeName.lastIndexOf('>') + 1;
			final String tmp = typeName.substring(0, indexOfLt) + (lastIndexOfGt == typeName.length() ? "" : typeName.substring(lastIndexOfGt));
			return "(Class<" + typeName + ">) (Class<?>) " + tmp;
		}
		return typeName;
	}

	public static String getTypeName(final TypeMirror type) {
		final TypeKind kind = type.getKind();
		String typeName = type.toString();
		if (kind.isPrimitive()) {
			switch (kind) {
				case CHAR:
					typeName = "java.lang.Character";
					break;
				case INT:
					typeName = "java.lang.Integer";
					break;
				// $CASES-OMITTED$
				default:
					final String s = type.toString();
					typeName = "java.lang." + s.substring(0, 1).toUpperCase() + s.substring(1);
					break;
			}
		} else {
			switch (kind) {
				case TYPEVAR:
					typeName = ((TypeVariable) type).getUpperBound().toString();
					break;
				// $CASES-OMITTED$
				default:
					typeName = type.toString();
					break;
			}
		}
		// in Java 11 there are annotations in typeName, for example:
		// (@javax.validation.constraints.Size(max=15, message="Sono permesse al massimo 15 righe di conti sciroppo"),@javax.validation.Valid :: dev.tacon.dto.reference.DtoIntReference<? extends dev.tacon.example.MyGenericType<?>>)
		// so we take the substring after the last whitespace character
		if (typeName.startsWith("(")) {
			typeName = typeName.substring(1);
			if (typeName.endsWith(")")) {
				typeName = typeName.substring(0, typeName.length() - 1);
			}
			final int doubleColonIndex = typeName.lastIndexOf(" :: ");
			if (doubleColonIndex > 0) {
				typeName = typeName.substring(doubleColonIndex + 4);
			}
		}
		// clear annotations
		if (typeName.contains("@")) {
			int spaceIx = -1;
			if (typeName.endsWith(">")) {
				int x = 0;
				for (int i = typeName.length() - 1; i >= 0; i--) {
					final char c = typeName.charAt(i);
					if (c == '>') {
						x++;
					} else if (c == '<') {
						x--;
					} else if (c == ' ' && x == 0) {
						spaceIx = i + 1;
						break;
					}
				}
			} else {
				spaceIx = typeName.lastIndexOf(" ") + 1;
			}
			typeName = typeName.substring(spaceIx);
		}

		return typeName;
	}

	private Utils() {}
}
