package dev.tacon.beans.apt;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;

public class Property {

	public TypeMirror type;
	public VariableElement field;
	public ExecutableElement getterMethod;
	public ExecutableElement setterMethod;

	@Override
	public String toString() {
		return this.type + " " + this.field + " " + this.getterMethod + " " + this.setterMethod;
	}

	public boolean isComplete() {
		return this.type != null && this.field != null && this.getterMethod != null;
	}

	public boolean isCompleteSetter() {
		return this.type != null && this.field != null && this.getterMethod != null && this.setterMethod != null;
	}
}