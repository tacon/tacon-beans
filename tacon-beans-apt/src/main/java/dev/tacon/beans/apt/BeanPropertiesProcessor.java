package dev.tacon.beans.apt;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import dev.tacon.beans.annotation.Bean;
import freemarker.template.Configuration;
import freemarker.template.Template;

@SupportedAnnotationTypes("dev.tacon.beans.annotation.Bean")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class BeanPropertiesProcessor extends AbstractProcessor {

	private String classSuffix = "_";
	private String additionalPackageName = "";
	private Template mainClassTemplate = null;
	private Template innerClassTemplate = null;

	@Override
	public synchronized void init(final ProcessingEnvironment environment) {
		super.init(environment);

		final Messager messager = environment.getMessager();
		final Map<String, String> options = environment.getOptions();

		messager.printMessage(Kind.NOTE, "Starting BeanProcessor");

		this.classSuffix = options.getOrDefault("classsuffix", "_");
		this.additionalPackageName = options.getOrDefault("additionalpackagename", "");

		final Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
		cfg.setClassForTemplateLoading(this.getClass(), "/");
		try {
			this.mainClassTemplate = cfg.getTemplate("bean.ftl");
			this.innerClassTemplate = cfg.getTemplate("inner-bean.ftl");
		} catch (final IOException e) {
			messager.printMessage(Kind.ERROR, e.getMessage());
		}
	}

	@Override
	public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
		final Set<TypeElement> typeElements = ElementFilter.typesIn(roundEnv.getElementsAnnotatedWith(Bean.class));
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "found " + typeElements.size() + " types");
		for (final TypeElement typeElement : typeElements) {
			this.writeSourceFile(typeElement);
		}
		return false;
	}

	private void writeSourceFile(final TypeElement typeElement) {
		try {
			final String typeElementContent = this.process(typeElement, this.mainClassTemplate);
			if (!typeElementContent.isEmpty()) {

				final String className = typeElement.getSimpleName().toString();
				final String classPackageName = this.processingEnv.getElementUtils().getPackageOf(typeElement).getQualifiedName().toString();
				final String destinationPackage = classPackageName + this.additionalPackageName;

				final JavaFileObject jfo = this.processingEnv.getFiler().createSourceFile(destinationPackage + "." + className + this.classSuffix);

				this.processingEnv.getMessager().printMessage(Kind.NOTE, "creating source file: " + jfo.toUri());

				try (final Writer writer = jfo.openWriter()) {
					writer.write(typeElementContent);
				}
			}
		} catch (final Exception ex) {
			final String msg = stackTrace(ex);
			this.processingEnv.getMessager().printMessage(Kind.WARNING, msg, typeElement);
		}
	}

	private static String stackTrace(final Exception ex) {
		String msg = null;
		final StringWriter stringWriter = new StringWriter();
		try (final PrintWriter printWriter = new PrintWriter(stringWriter)) {
			ex.printStackTrace(printWriter);
			msg = stringWriter.toString();
		}
		return msg;
	}

	private String process(final TypeElement typeElement, final Template template) throws Exception {
		final List<String> innerClasses = new ArrayList<>();
		for (final TypeElement innerTypeElement : ElementFilter.typesIn(typeElement.getEnclosedElements())) {
			if (innerTypeElement.getAnnotation(Bean.class) != null) {
				innerClasses.add(this.process(innerTypeElement, this.innerClassTemplate));
			}
		}
		return this.process(typeElement, template, innerClasses);
	}

	private String process(final TypeElement typeElement, final Template template, final List<String> innerClassesContent) throws Exception {

		final String className = typeElement.getQualifiedName().toString();
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "processing class " + className);

		final Map<String, Property> map = new HashMap<>();
		final List<ExecutableElement> methods = ElementFilter.methodsIn(typeElement.getEnclosedElements());

		for (final ExecutableElement method : methods) {
			final String methodName = method.getSimpleName().toString();
			if (method.getModifiers().contains(Modifier.PUBLIC) && !method.getModifiers().contains(Modifier.STATIC)
					&& method.getParameters().isEmpty() && methodName.startsWith("get")
					|| methodName.startsWith("is")) {
				final String fieldName = Utils.getFieldName(methodName);
				final Property p = new Property();
				p.getterMethod = method;
				p.type = method.getReturnType();
				map.put(fieldName, p);
			}
		}

		final Types typeUtils = this.processingEnv.getTypeUtils();

		for (final ExecutableElement method : methods) {
			final String methodName = method.getSimpleName().toString();
			if (method.getModifiers().contains(Modifier.PUBLIC) && !method.getModifiers().contains(Modifier.STATIC)
					&& method.getParameters().size() == 1 && methodName.startsWith("set")) {
				final String fieldName = Utils.getFieldName(methodName);
				final Property property = map.get(fieldName);
				if (property != null) {
					final VariableElement parameter = method.getParameters().iterator().next();
					final TypeMirror type = parameter.asType();
					if (typeUtils.isSameType(type, property.type)) {
						property.setterMethod = method;
					} else {
						map.remove(fieldName);
					}
				}
			}
		}
		final List<VariableElement> fields = ElementFilter.fieldsIn(typeElement.getEnclosedElements());
		for (final VariableElement field : fields) {
			if (!field.getModifiers().contains(Modifier.STATIC)) {
				final String fieldName = field.getSimpleName().toString();
				final Property property = map.get(fieldName);
				if (property != null) {
					final TypeMirror type = field.asType();
					if (typeUtils.isSameType(type, property.type)) {
						property.field = field;
					} else {
						map.remove(fieldName);
					}
				}
			}
		}

		final Iterator<Entry<String, Property>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			final Property property = it.next().getValue();
			if (!property.isComplete()) {
				it.remove();
			}
		}

		this.processingEnv.getMessager().printMessage(Kind.NOTE, "processing " + map.size() + " fields for class " + className);

		if (!map.isEmpty()) {
			return this.generateClass(typeElement, map, template, innerClassesContent);
		}
		return "";
	}

	private String generateClass(final TypeElement typeElement, final Map<String, Property> map, final Template template, final List<String> innerClassesContent) throws Exception {
		final String fullClassName = typeElement.getQualifiedName().toString();
		this.processingEnv.getMessager().printMessage(Kind.NOTE, "writing metadata class for " + fullClassName);

		final String className = typeElement.getSimpleName().toString();
		final String classPackageName = this.processingEnv.getElementUtils().getPackageOf(typeElement).getQualifiedName().toString();
		final String destinationPackage = classPackageName + this.additionalPackageName;

		final Map<String, Object> vc = new HashMap<>();

		vc.put("classSuffix", this.classSuffix);
		vc.put("fullClassName", fullClassName);
		vc.put("className", className);
		vc.put("classPackageName", classPackageName);
		vc.put("packageName", destinationPackage);
		vc.put("fields", map.entrySet().stream().map(this::createBeanField).collect(Collectors.toList()));
		vc.put("innerClasses", innerClassesContent);

		try (StringWriter sw = new StringWriter()) {
			template.process(vc, sw);
			return sw.toString();
		}
	}

	private BeanField createBeanField(final Entry<String, Property> e) {
		final VariableElement field = e.getValue().field;
		final String fieldName = field.getSimpleName().toString();
		final TypeMirror type = field.asType();
		final String typeName = Utils.getTypeName(type);
		String classTypeName = typeName;

		final int indexOfLt = typeName.indexOf('<');
		if (indexOfLt > 0) {
			// XXX from "List<List<String>>[]" to "List[]"
			final int lastIndexOfGt = typeName.lastIndexOf('>') + 1;
			final String tmp = typeName.substring(0, indexOfLt) + (lastIndexOfGt == typeName.length() ? "" : typeName.substring(lastIndexOfGt));
			classTypeName = "(Class<" + typeName + ">) (Class<?>) " + tmp;
		} else if (type.getKind().isPrimitive()) {
			// we need to pass int.class instead of Integer.class, for generics we use <Integer>
			classTypeName = type.toString();
		}

		return new BeanField(fieldName, typeName, classTypeName);
	}

	public static class BeanField {

		private final String name;
		private final String type;
		private final String classType;

		public BeanField(final String name, final String type, final String classType) {
			super();
			this.name = name;
			this.type = type;
			this.classType = classType;
		}

		public String getName() {
			return this.name;
		}

		public String getType() {
			return this.type;
		}

		public String getClassType() {
			return this.classType;
		}
	}
}