package dev.tacon.beans.annotation;

/**
 * Represents a bean's property reference.
 * <p>
 * Is considered "property" every bean's field having
 * an accessor (getter) method (is/get).
 *
 * @param <B> type of the bean
 * @param <T> type of the property
 */
public class BeanProperty<A, P, B, T> {

	public static class RootBeanProperty<B, T> extends BeanProperty<B, Void, B, T> {

		public RootBeanProperty(final Class<B> beanType, final String name, final Class<T> type) {
			super(null, beanType, name, type);
		}
	}

	final BeanProperty<A, ?, P, B> parent;
	final Class<B> beanType;
	final String name;
	final Class<T> type;

	protected BeanProperty(final BeanProperty<A, ?, P, B> parent, final Class<B> beanType, final String name, final Class<T> type) {
		this.parent = parent;
		this.beanType = beanType;
		this.name = name;
		this.type = type;
	}

	public Class<B> getBeanType() {
		return this.beanType;
	}

	public String getName() {
		return this.name;
	}

	public Class<T> getType() {
		return this.type;
	}

	public BeanProperty<A, ?, P, B> getParent() {
		return this.parent;
	}

	public <X> BeanProperty<A, B, T, X> then(final BeanProperty<?, ?, T, X> property) {
		return new BeanProperty<>(this, property.beanType, property.name, property.type);
	}

	public String getNestedName() {
		if (this.parent == null) {
			return this.name;
		}
		return this.parent.getNestedName() + "." + this.name;
	}

	@Override
	public String toString() {
		return this.getNestedName();
	}
}
